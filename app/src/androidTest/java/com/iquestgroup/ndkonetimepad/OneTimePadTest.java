package com.iquestgroup.ndkonetimepad;

import android.os.Environment;
import android.test.AndroidTestCase;
import android.text.TextUtils;
import com.iquestgroup.ndkonetimepad.processor.EncryptionProcessor;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.security.SecureRandom;

/**
 * Unit test for the happy flow for encryption/decryption.
 * In the case of a one-time pad encryption:
 * text ^ key = ciphertext
 * ciphertext ^ key = text
 * Therefore, after an encrypt and then a decrypt on a text with the same key,
 * we should be able to extract the original text.
 *
 * @author victor.rad on 26.01.2015.
 */
public class OneTimePadTest extends AndroidTestCase {

  private EncryptionProcessor processorUnderTest;

  private static final String PLAINTEXT = "The quick brown fox jumps over the lazy dog.0123456789[]{}_+-="
      + "aaaaaaaaaaaaaaaaaaaaaabbbbbbbbbbbbbbbbbbbbbbbccccccccccccccccccccccdddddddddddddddddddd"
      + "eeeeeeeeeeefffffffffffffAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAscpscpscpscpscpscpscpscpblahblahblahblah"
      + "raindropskeepfallingonmyhead1234567890MY BRAIN HURTSggggggggggggggggggggggggggggggggajmtrhigegge"
      + "asnrh4iugtiut5brohgoi45pjrgpohpohiphgfdogphurpsegviypgiwpgfocibrpwvipcg9381g086 0-170y98y391pf9p1"
      + "sndoag9eg02g1iueh0d=hrnv2g;f59p47fy3=2rjtvb1]hg5=h= b9gh4=u09eghkgjn0y2g=5ithgp'nsodpjf-0ug=3g";

  private static final String BASE_PATH = Environment.getExternalStorageDirectory().getAbsolutePath();
  private static final String INPUT_PATH = BASE_PATH + "/plaintext.txt";
  private static final String KEY_PATH = BASE_PATH + "/otpkey.txt";
  private static final String CIPHERTEXT_PATH = BASE_PATH + "/ciphertext.txt";
  private static final String OUTPUT_PATH = BASE_PATH + "/outputtext.txt";

  public void setUp() throws Exception {
    processorUnderTest = new EncryptionProcessor();
    String key = generateKey();
    writeToFile(INPUT_PATH, PLAINTEXT);
    writeToFile(KEY_PATH, key);
  }

  private void writeToFile(String filePath, String text) {
    try {
      File file = new File(filePath);
      if (!file.exists()) {
        file.createNewFile();
      }
      BufferedWriter writer = new BufferedWriter(new FileWriter(file));
      writer.write(text);
      writer.flush();
      writer.close();
    }
    catch (Exception e) {
      fail(e.getMessage());
    }
  }


  private String generateKey() {
    String key = "";
    SecureRandom random = new SecureRandom();
    for (int i=0; i<10000; i++) {
      key += Character.toChars(random.nextInt(65536));
    }
    return key;
  }

  public void testOneTimePad() {
    try {
      processorUnderTest.encryptData(INPUT_PATH, KEY_PATH, CIPHERTEXT_PATH, 0);
      processorUnderTest.decryptData(CIPHERTEXT_PATH, KEY_PATH, false, OUTPUT_PATH);
      // let's call them Alice and Bob because that's what they're always called in crypto problems :D
      File aliceEndFile = new File(INPUT_PATH);
      File bobEndFile = new File(OUTPUT_PATH);
      BufferedReader aliceReader = new BufferedReader(new FileReader(aliceEndFile));
      BufferedReader bobReader = new BufferedReader(new FileReader(bobEndFile));
      String aliceFile = "";
      String aliceLine;
      String bobFile = "";
      String bobLine;
      while ((aliceLine = aliceReader.readLine()) != null) {
        aliceFile += aliceLine;
      }
      while ((bobLine = bobReader.readLine()) != null) {
        bobFile += bobLine;
      }
      assertTrue(TextUtils.equals(aliceFile, bobFile));
      aliceReader.close();
      bobReader.close();
    }
    catch (Exception e) {
      fail(e.getMessage());
    }
  }

  public void tearDown() throws Exception {
    processorUnderTest = null;
  }

}
