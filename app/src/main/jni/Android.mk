LOCAL_PATH          := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_CPP_EXTENSION := .cpp

LOCAL_MODULE        := onetimepad
LOCAL_SRC_FILES     := onetimepad.cpp
LOCAL_LDLIBS        := -llog

include $(BUILD_SHARED_LIBRARY)