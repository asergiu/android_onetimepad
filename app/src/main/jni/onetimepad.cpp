/* 
OTP varianta 1.2
one time pad full duplex
offset = variabil, pozitiv sau negativ
md5 works
optimized buffer size
abort key when reached keysize/2
key file cannot be overwritten
*/
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <jni.h>
#include <android/log.h>

#define BUFFER_SIZE 20000 // buffer size

jint throwIoException(JNIEnv *env, const char *message) {
    jclass ioExceptionClass;
    const char *className = "java/io/IOException";

    ioExceptionClass = env->FindClass(className);
    return env->ThrowNew(ioExceptionClass, message);
}

int encrypt(JNIEnv *env, const char *source, const char *key, unsigned long offset, const char *destination) {
    FILE *fsource;
    FILE *fdestination;
    FILE *fkey;

    if (!strcmp(key, destination)) {
        throwIoException(env, "Key and destination files must not be identical");
        return -1;
    }

    fsource = fopen(source, "rb");
    if (fsource == NULL) {
        __android_log_print(ANDROID_LOG_ERROR, "onetimepad", "Could not open source file %s", source);
        throwIoException(env, "Could not open source file");
        return -1;
    }
    fdestination = fopen(destination, "w+b");
    if (fdestination == NULL) {
        __android_log_print(ANDROID_LOG_ERROR, "onetimepad", "Could not open destination file %s", source);
        throwIoException(env, "Could not open destination file");
        return -1;
    }
    fkey = fopen(key, "rb");
    if (fkey == NULL) {
        __android_log_print(ANDROID_LOG_ERROR, "onetimepad", "Could not open key file %s", source);
        throwIoException(env, "Could not open key file");
        return -1;
    }

    unsigned char inputBuffer[BUFFER_SIZE];
    unsigned char keyBuffer[BUFFER_SIZE];
    unsigned long inputSize;
    unsigned long keySize;
    long keyOffset = offset;

    fseek(fsource, 0, SEEK_END);
    inputSize = ftell(fsource);
    fseek(fsource, 0, SEEK_SET);

    fseek(fkey, 0, SEEK_END);
    keySize = ftell(fkey);
    fseek(fkey, 0, SEEK_SET);

    __android_log_print(ANDROID_LOG_DEBUG, "onetimepad", "offset is %d, input size %lu and keySize %lu", abs(offset), inputSize, keySize);
    if (abs(keyOffset) + inputSize > (keySize / 2)) {
        __android_log_print(ANDROID_LOG_ERROR, "onetimepad", "Not enough OTP key in file %s for encryption, stopping", key);
    	fclose(fdestination);
        return offset;
    }

    if (offset >= 0) {
        fseek(fkey, keyOffset, SEEK_SET);
    }
    else {
	    fseek(fkey, keyOffset, SEEK_END);
    }

    long bufferFill;
    long keyFill;

    fprintf(fdestination, "%ld ", keyOffset);
    fprintf(fdestination, "%s ", source);

    while (true) {
    	bufferFill = fread(inputBuffer, 1, BUFFER_SIZE, fsource);
    	__android_log_print(ANDROID_LOG_DEBUG, "onetimepad", "bufferFill from input file is %ld", bufferFill);
    	if (bufferFill == -1) {
    	    throwIoException(env, "Error reading from input file");
    	    return -1;
    	}
    	if (bufferFill == 0) {
    	    __android_log_print(ANDROID_LOG_DEBUG, "onetimepad", "End of source file");
    	    break;
        }
    	keyFill = fread(keyBuffer, 1, BUFFER_SIZE, fkey);
    	__android_log_print(ANDROID_LOG_DEBUG, "onetimepad", "keyFill from input file is %ld", keyFill);
    	if (keyFill == -1) {
    	    throwIoException(env, "Error reading from key file");
    	    return -1;
    	}
    	if (bufferFill > keyFill) {
    	    __android_log_print(ANDROID_LOG_ERROR, "onetimepad", "bufferFill != keyFill (%ld != %ld))", bufferFill, keyFill);
    	    throwIoException(env, "Desynchronization between source and key streams");
    	    return -1;
    	}
        for (int i=0; i<bufferFill; i++) {
    		inputBuffer[i] = inputBuffer[i] ^ keyBuffer[i]; /* aplicare xor */
        }
        fwrite(inputBuffer, 1, bufferFill, fdestination);
    	keyOffset += bufferFill;
    	__android_log_print(ANDROID_LOG_DEBUG, "onetimepad", "Offset increased to %ld", keyOffset);
    }

    fclose(fsource);
    fclose(fdestination);
    fclose(fkey);

    return keyOffset;
}

int decrypt(JNIEnv *env, const char *source, const char *key, int saveBackToOriginal, const char *alternateDestination) {
    FILE *fsource;
    FILE *fdestination;
    FILE *fkey;

    unsigned char inputBuffer[BUFFER_SIZE];
    unsigned char keyBuffer[BUFFER_SIZE];
    unsigned long inputSize;
    unsigned long keySize;
    unsigned long keyOffset;

    fsource = fopen(source, "rb");
    if (fsource == NULL) {
        __android_log_print(ANDROID_LOG_ERROR, "onetimepad", "Could not open source file %s", source);
        throwIoException(env, "Could not open source file");
        return -1;
    }

    char destination[200];
    fscanf(fsource, "%ld", &keyOffset);
    fscanf(fsource, "%s", destination);
    fdestination = fopen((saveBackToOriginal == 1 ? destination : alternateDestination), "w+b");

    if (fdestination == NULL) {
        __android_log_print(ANDROID_LOG_ERROR, "onetimepad", "Could not open destination file %s", source);
        throwIoException(env, "Could not open destination file");
        return -1;
    }

    fkey = fopen(key, "rb");
    if (fkey == NULL) {
        __android_log_print(ANDROID_LOG_ERROR, "onetimepad", "Could not open key file %s", source);
        throwIoException(env, "Could not open key file");
        return -1;
    }

    if (keyOffset >= 0) {
        fseek(fkey, keyOffset, SEEK_SET);
    }
    else {
        fseek(fkey, keyOffset, SEEK_END);
    }

    fseek(fsource, 1, SEEK_CUR);

    long bufferFill;
    long keyFill;
    while (true) {
        bufferFill = fread(inputBuffer, 1, BUFFER_SIZE, fsource);
        if (bufferFill == -1) {
    	    throwIoException(env, "Error reading from input file");
    	    return -1;
        }
    	if (bufferFill == 0) {
    	    __android_log_print(ANDROID_LOG_DEBUG, "onetimepad", "End of source file");
    	    break;
        }
        keyFill = fread(keyBuffer, 1, BUFFER_SIZE, fkey);
        if (keyFill == -1) {
            throwIoException(env, "Error reading from key file");
            return -1;
        }
        if (bufferFill > keyFill) {
    	    __android_log_print(ANDROID_LOG_ERROR, "onetimepad", "bufferFill != keyFill (%ld != %ld))", bufferFill, keyFill);
    	    throwIoException(env, "Desynchronization between source and key streams");
    	    return -1;
        }
        for (int i=0; i<bufferFill; i++) {
    		inputBuffer[i] = inputBuffer[i] ^ keyBuffer[i]; /* aplicare xor */
        }
        fwrite(inputBuffer, 1, bufferFill, fdestination);
        keyOffset += bufferFill;
    }

    fclose(fsource);
    fclose(fkey);
    fclose(fdestination);

    return keyOffset;
}

char *getNameForOriginalPlaintextFile(JNIEnv *env, const char *source) {
    FILE *fsource;
    fsource = fopen(source, "rb");
    if (fsource == NULL) {
        __android_log_print(ANDROID_LOG_ERROR, "onetimepad", "Could not open source file %s", source);
        throwIoException(env, "Could not open source file");
    }

    long keyOffset;
    char *destination = (char *) malloc(200 * sizeof(char));
    fscanf(fsource, "%ld", &keyOffset);
    fscanf(fsource, "%s", destination);

    return destination;
}

extern "C" {

    JNIEXPORT jlong JNICALL Java_com_iquestgroup_ndkonetimepad_processor_EncryptionProcessor_encryptData
    (JNIEnv *env, jobject *thiz, jstring pathToSource, jstring pathToKey, jstring pathToDest, jlong keyOffset) {
        const char *nativePathToSource = env->GetStringUTFChars(pathToSource, NULL);
        const char *nativePathToKey = env->GetStringUTFChars(pathToKey, NULL);
        const char *nativePathToDest = env->GetStringUTFChars(pathToDest, NULL);

        long result = encrypt(env, nativePathToSource, nativePathToKey, (long) keyOffset, nativePathToDest);
        env->ReleaseStringUTFChars(pathToSource, nativePathToSource);
        env->ReleaseStringUTFChars(pathToKey, nativePathToKey);
        env->ReleaseStringUTFChars(pathToDest, nativePathToDest);
        return result;
    }

    JNIEXPORT jlong JNICALL Java_com_iquestgroup_ndkonetimepad_processor_EncryptionProcessor_decryptData
    (JNIEnv *env, jobject *thiz, jstring pathToSource, jstring pathToKey, jboolean saveBackToOriginal, jstring alternatePathToDest) {
        const char *nativePathToSource = env->GetStringUTFChars(pathToSource, NULL);
        const char *nativePathToKey = env->GetStringUTFChars(pathToKey, NULL);
        const char *nativeAlternateDestination = env->GetStringUTFChars(alternatePathToDest, NULL);

        int saveBack = (saveBackToOriginal == JNI_TRUE) ? 1 : 0;
        long result = decrypt(env, nativePathToSource, nativePathToKey, saveBack, nativeAlternateDestination);
        env->ReleaseStringUTFChars(pathToSource, nativePathToSource);
        env->ReleaseStringUTFChars(pathToKey, nativePathToKey);
        env->ReleaseStringUTFChars(alternatePathToDest, nativeAlternateDestination);
        return result;
    }

    JNIEXPORT jstring JNICALL Java_com_iquestgroup_ndkonetimepad_processor_EncryptionProcessor_getOriginalPlaintextFile
    (JNIEnv *env, jobject *thiz, jstring pathToSource) {
         const char *nativePathToSource = env->GetStringUTFChars(pathToSource, NULL);
         char *result = getNameForOriginalPlaintextFile(env, nativePathToSource);
         env->ReleaseStringUTFChars(pathToSource, nativePathToSource);
         jstring jresult = env->NewStringUTF(result);
         return jresult;
    }

}