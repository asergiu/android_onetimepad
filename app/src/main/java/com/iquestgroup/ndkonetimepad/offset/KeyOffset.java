package com.iquestgroup.ndkonetimepad.offset;

/**
 * POJO used for storing data about a key that has been used so far in the application.
 *
 * @author victor.rad on 28.01.2015.
 */
public class KeyOffset {

  private String keyFilePath;
  private long offset;
  private long keySize;

  public KeyOffset() {

  }

  public String getKeyFilePath() {
    return keyFilePath;
  }

  public void setKeyFilePath(String keyFilePath) {
    this.keyFilePath = keyFilePath;
  }

  public long getOffset() {
    return offset;
  }

  public void setOffset(long offset) {
    this.offset = offset;
  }

  public long getKeySize() {
    return keySize;
  }

  public void setKeySize(long keySize) {
    this.keySize = keySize;
  }

  @Override
  public String toString() {
    return "Key file path: " + keyFilePath + " | Amount of key used: " + offset + "/" + keySize + " characters";
  }
}
