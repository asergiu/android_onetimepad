package com.iquestgroup.ndkonetimepad.offset;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

/**
 * Wrapper around PrefsController used to store the key
 *
 * @author victor.rad on 28.01.2015.
 */
public class KeyOffsetController {

  private static final String PREFERENCES_FILE_NAME = "KeyPreferences";

  private static final String KEY_OFFSET_LIST_KEY = "KeyOffsets";

  private SharedPreferences preferences;

  public KeyOffsetController(Context context) {
    preferences = context.getSharedPreferences(PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
  }

  public void addOrUpdateKey(KeyOffset offset) {
    List<KeyOffset> keyOffsets = getListOfKeys();
    boolean keyAlreadyAdded = false;
    for (KeyOffset oldOffset : keyOffsets) {
      if (TextUtils.equals(oldOffset.getKeyFilePath(), offset.getKeyFilePath())) {
        oldOffset.setKeySize(offset.getKeySize());
        oldOffset.setOffset(offset.getOffset());
        keyAlreadyAdded = true;
      }
    }
    if (!keyAlreadyAdded) {
      keyOffsets.add(offset);
    }
    preferences.edit().putString(KEY_OFFSET_LIST_KEY, new Gson().toJson(keyOffsets)).apply();
  }

  public long getOffsetForKeyWithPath(String path) {
    List<KeyOffset> keyOffsets = getListOfKeys();
    for (KeyOffset offset : keyOffsets) {
      if (TextUtils.equals(path, offset.getKeyFilePath())) {
        return offset.getOffset();
      }
    }
    return 0;
  }

  public List<KeyOffset> getListOfKeys() {
    List<KeyOffset> keyOffsets = new ArrayList<>();
    String serializedOffsets = preferences.getString(KEY_OFFSET_LIST_KEY, "");
    if (!TextUtils.isEmpty(serializedOffsets)) {
      keyOffsets = new Gson().fromJson(serializedOffsets, new TypeToken<ArrayList<KeyOffset>>() {
      }.getType());
    }
    return keyOffsets;
  }
}
