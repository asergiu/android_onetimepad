package com.iquestgroup.ndkonetimepad.processor;

import java.io.IOException;

/**
 * Container class for the encryption native methods in order to make them testable.
 *
 * @author victor.rad on 26.01.2015.
 */
public class EncryptionProcessor {

  /**
   * Performs native one-time pad encryption of a file.
   * Encryption key is being read starting from a given position in the key file. The position advances as the key
   * and source are read in parallel.
   *
   * @param pathToSource path to source file in storage
   * @param pathToKey    path to key file in storage
   * @param pathToDest   path to destination file in storage
   * @param keyOffset    offset in key file from which to start encryption
   * @return new offset in key file after encryption, or -1 if an error occured
   * @throws java.io.IOException when source/key/destination files cannot be read/written to
   */
  public native long encryptData(String pathToSource, String pathToKey, String pathToDest, long keyOffset)
      throws IOException;

  /**
   * Performs native one-time pad decryption of a file.
   * Decryption destination and key offset for decryption are read from the start of the source file, where they are
   * saved during the encryption operation.
   *
   * @param pathToSource        path to source (encrypted) file in storage
   * @param pathToKey           path to key file in storage
   * @param saveBackToOriginal  whether to save back to the original file that was encrypted initially
   * @param alternatePathToDest path to save to if we want to save to a different file
   * @return end offset after decryption, or -1 if an error occured
   * @throws IOException when source/key/destination files cannot be read/written to
   */
  public native long decryptData(String pathToSource, String pathToKey, boolean saveBackToOriginal,
      String alternatePathToDest) throws IOException;


  /**
   * Retrieves the path to the original plaintext file from the header of a ciphertext file.
   *
   * @param pathToDecryptionSource path to file in storage containing ciphertext
   * @return path to original plaintext file
   */
  public native String getOriginalPlaintextFile(String pathToDecryptionSource);
}
