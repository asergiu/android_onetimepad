package com.iquestgroup.ndkonetimepad.processor;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;
import com.iquestgroup.ndkonetimepad.callback.OperationCompletedBroadcastReceiver;
import com.iquestgroup.ndkonetimepad.callback.OperationFailedBroadcastReceiver;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * IntentService which performs file encryption in the background.
 *
 * @author victor.rad on 26.01.2015.
 */
public class EncryptionService extends IntentService {

  public static final String SOURCE_FILE_PATH = "SourceFilePath";
  public static final String DEST_FILE_PATH = "DestFilePath";
  public static final String KEY_FILE_PATH = "KeyFilePath";
  public static final String OFFSET_EXTRA = "OffsetExtra";
  public static final String SAVE_BACK_EXTRA = "SaveBackToOriginal";

  public static final String OPERATION = "Operation";

  public EncryptionService() {
    super(EncryptionService.class.getSimpleName());
  }

  private EncryptionProcessor processor;

  @Override
  protected void onHandleIntent(Intent intent) {
    processor = new EncryptionProcessor();

    String sourceFilePath = intent.getStringExtra(SOURCE_FILE_PATH);
    String destFilePath = intent.getStringExtra(DEST_FILE_PATH);
    String keyFilePath = intent.getStringExtra(KEY_FILE_PATH);
    long offset = intent.getLongExtra(OFFSET_EXTRA, 0);
    boolean saveBack = intent.getBooleanExtra(SAVE_BACK_EXTRA, false);
    Operation operation = (Operation) intent.getSerializableExtra(OPERATION);

    switch (operation) {
      case ENCRYPT:
        performEncryption(sourceFilePath, keyFilePath, destFilePath, offset);
        break;
      case DECRYPT:
        performDecryption(sourceFilePath, keyFilePath, destFilePath, saveBack);
        break;
    }
  }

  private void performEncryption(String pathToSource, String pathToKey, String pathToDest, long keyOffset) {
    try {
      long newOffset = processor.encryptData(pathToSource, pathToKey, pathToDest, keyOffset);
      sendCompletionBroadcast(pathToSource, pathToDest, newOffset);
    }
    catch (IOException e) {
      Log.e("EncryptionService", "Error in encrypting file", e);
      sendErrorBroadcastWithException(e);
    }
  }

  private void performDecryption(String sourceFilePath, String keyFilePath, String destFilePath, boolean saveBack) {
    try {
      long offsetAfterDecryption = processor.decryptData(sourceFilePath, keyFilePath, saveBack, destFilePath);
      String originalDestPath = processor.getOriginalPlaintextFile(sourceFilePath);
      sendCompletionBroadcast(sourceFilePath, saveBack ? originalDestPath : destFilePath, offsetAfterDecryption);
    }
    catch (IOException e) {
      sendErrorBroadcastWithMessage("Not enough key remaining for decryption");
    }
  }

  private void sendCompletionBroadcast(String pathToSource, String pathToDest, long newOffset) {
    Intent completionIntent = new Intent(this, OperationCompletedBroadcastReceiver.class);
    completionIntent.putExtra(OperationCompletedBroadcastReceiver.NEW_OFFSET, newOffset);
    completionIntent.putExtra(OperationCompletedBroadcastReceiver.INPUT_TEXT, getContentFromPath(pathToSource));
    completionIntent.putExtra(OperationCompletedBroadcastReceiver.OUTPUT_TEXT, getContentFromPath(pathToDest));
    sendBroadcast(completionIntent);
  }

  private void sendErrorBroadcastWithMessage(String message) {
    Intent errorIntent = new Intent(this, OperationFailedBroadcastReceiver.class);
    errorIntent.putExtra(OperationFailedBroadcastReceiver.MESSAGE, message);
    sendBroadcast(errorIntent);
  }

  private void sendErrorBroadcastWithException(IOException e) {
    Intent errorIntent = new Intent(this, OperationFailedBroadcastReceiver.class);
    errorIntent.putExtra(OperationFailedBroadcastReceiver.MESSAGE, e.getMessage());
    sendBroadcast(errorIntent);
  }

  public String getContentFromPath(String filePath) {
    String result = "";
    try {
      BufferedReader reader = new BufferedReader(new FileReader(filePath));
      String line;
      while ((line = reader.readLine()) != null) {
        result += line;
      }
      reader.close();
    }
    catch (IOException e) {
      Log.e("EncryptionService", "Error in reading file", e);
      e.printStackTrace();
      return "Cannot read file with path " + filePath;
    }
    return result;
  }

  public enum Operation {
    ENCRYPT,
    DECRYPT,
    NONE
  }
}
