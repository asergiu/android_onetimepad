package com.iquestgroup.ndkonetimepad.callback;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @author victor.rad on 26.01.2015.
 */
public class OperationFailedBroadcastReceiver extends BroadcastReceiver {

  public static final String MESSAGE = "Message";

  private static ConcurrentMap<String, CopyOnWriteArrayList<Listener>> listeners = new ConcurrentHashMap<>();

  public static void registerListener(String listenerTag, Listener listener) {
    CopyOnWriteArrayList<Listener> listOfListenersForKey;
    if (listeners.containsKey(listenerTag)) {
      listOfListenersForKey = listeners.get(listenerTag);
    }
    else {
      listOfListenersForKey = new CopyOnWriteArrayList<>();
    }
    listOfListenersForKey.add(listener);
    listeners.put(listenerTag, listOfListenersForKey);
  }

  public static void unregisterListener(String listenerTag, Listener listener) {
    if (!listeners.containsKey(listenerTag)) {
      return;
    }
    CopyOnWriteArrayList<Listener> listOfListenersForKey = listeners.get(listenerTag);
    listOfListenersForKey.remove(listener);
    if (listOfListenersForKey.isEmpty()) {
      listeners.remove(listenerTag);
    }
  }

  @Override
  public void onReceive(Context context, Intent intent) {
    String errorMessage = intent.getStringExtra(MESSAGE);
    broadcast(errorMessage);
  }

  private void broadcast(String errorMessage) {
    for (String identifier : listeners.keySet()) {
      if (identifier != null) {
        CopyOnWriteArrayList<Listener> listOfListenersForKey = listeners.get(identifier);
        for (Listener listener : listOfListenersForKey) {
          if (listener != null) {
            listener.onOperationFailed(errorMessage);
          }
        }
      }
    }
  }

  public interface Listener {

    void onOperationFailed(String errorMessage);

  }
}
