package com.iquestgroup.ndkonetimepad.callback;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * BroadcastReceiver fired when an encrypt/decrypt operation is completed successfully.
 *
 * @author victor.rad on 26.01.2015.
 */
public class OperationCompletedBroadcastReceiver extends BroadcastReceiver {

  public static final String NEW_OFFSET = "NewOffset";
  public static final String INPUT_TEXT = "InputText";
  public static final String OUTPUT_TEXT = "OutputText";

  private static ConcurrentMap<String, CopyOnWriteArrayList<Listener>> listeners = new ConcurrentHashMap<>();

  public static void registerListener(String listenerTag, Listener listener) {
    CopyOnWriteArrayList<Listener> listOfListenersForKey;
    if (listeners.containsKey(listenerTag)) {
      listOfListenersForKey = listeners.get(listenerTag);
    }
    else {
      listOfListenersForKey = new CopyOnWriteArrayList<>();
    }
    listOfListenersForKey.add(listener);
    listeners.put(listenerTag, listOfListenersForKey);
  }

  public static void unregisterListener(String listenerTag, Listener listener) {
    if (!listeners.containsKey(listenerTag)) {
      return;
    }
    CopyOnWriteArrayList<Listener> listOfListenersForKey = listeners.get(listenerTag);
    listOfListenersForKey.remove(listener);
    if (listOfListenersForKey.isEmpty()) {
      listeners.remove(listenerTag);
    }
  }

  @Override
  public void onReceive(Context context, Intent intent) {
    long newOffset = intent.getLongExtra(NEW_OFFSET, -1);
    String inputText = intent.getStringExtra(INPUT_TEXT);
    String outputText = intent.getStringExtra(OUTPUT_TEXT);
    broadcast(newOffset, inputText, outputText);
  }

  private void broadcast(long newOffset, String inputText, String outputText) {
    for (String identifier : listeners.keySet()) {
      if (identifier != null) {
        CopyOnWriteArrayList<Listener> listOfListenersForKey = listeners.get(identifier);
        for (Listener listener : listOfListenersForKey) {
          if (listener != null) {
            listener.onOperationCompleted(newOffset, inputText, outputText);
          }
        }
      }
    }
  }

  public interface Listener {

    void onOperationCompleted(long newKeyOffset, String inputText, String outputText);

  }
}
