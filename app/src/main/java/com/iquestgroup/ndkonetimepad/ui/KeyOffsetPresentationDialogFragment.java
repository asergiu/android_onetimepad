package com.iquestgroup.ndkonetimepad.ui;

import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.iquestgroup.ndkonetimepad.R;
import com.iquestgroup.ndkonetimepad.offset.KeyOffset;
import com.iquestgroup.ndkonetimepad.offset.KeyOffsetController;

/**
 * DialogFragment which displays the key files that have been used so far and how much of each key remains usable.
 *
 * @author victor.rad on 28.01.2015.
 */
public class KeyOffsetPresentationDialogFragment extends DialogFragment {

  private KeyOffsetController controller;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    controller = new KeyOffsetController(getActivity());
    setCancelable(true);
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    return inflater.inflate(R.layout.fragment_key_offset_dialog, container, false);
  }

  @Override
  public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    TextView keyListTextView = (TextView) view.findViewById(R.id.key_info_text_view);
    keyListTextView.setText(constructKeyDataText());
  }

  private String constructKeyDataText() {
    String result = "";
    for (KeyOffset keyOffset : controller.getListOfKeys()) {
      result += keyOffset.toString() + "\n\n";
    }
    return result;
  }
}
