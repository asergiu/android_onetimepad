package com.iquestgroup.ndkonetimepad.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.iquestgroup.ndkonetimepad.R;
import com.iquestgroup.ndkonetimepad.callback.OperationCompletedBroadcastReceiver;
import com.iquestgroup.ndkonetimepad.callback.OperationFailedBroadcastReceiver;
import com.iquestgroup.ndkonetimepad.offset.KeyOffset;
import com.iquestgroup.ndkonetimepad.offset.KeyOffsetController;
import com.iquestgroup.ndkonetimepad.processor.EncryptionService;

import java.io.File;

public class MainActivity extends ActionBarActivity implements View.OnClickListener,
    OperationCompletedBroadcastReceiver.Listener, OperationFailedBroadcastReceiver.Listener {

  private final String basePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/";
  private final String listenerTag = MainActivity.class.getSimpleName();

  private EditText inputFilePathEditText;
  private EditText keyFilePathEditText;
  private EditText outputFilePathEditText;
  private EditText keyOffsetEditText;
  private CheckBox saveBackToSourceCheckBox;

  private TextView inputTextView;
  private TextView outputTextView;

  private ProgressBar progressBar;
  private Button encryptButton;
  private Button decryptButton;

  private EncryptionService.Operation operationInProgress = EncryptionService.Operation.NONE;

  private KeyOffsetController controller;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    controller = new KeyOffsetController(this);
    initViews();
  }

  private void initViews() {
    inputFilePathEditText = (EditText) findViewById(R.id.input_file_edit_text);
    inputFilePathEditText.setHint(basePath);
    inputFilePathEditText.setText(basePath);

    keyFilePathEditText = (EditText) findViewById(R.id.key_file_edit_text);
    keyFilePathEditText.setHint(basePath);
    keyFilePathEditText.setText(basePath);

    outputFilePathEditText = (EditText) findViewById(R.id.output_file_edit_text);
    outputFilePathEditText.setHint(basePath);
    outputFilePathEditText.setText(basePath);

    keyOffsetEditText = (EditText) findViewById(R.id.key_offset_edit_text);

    inputTextView = (TextView) findViewById(R.id.input_text);
    outputTextView = (TextView) findViewById(R.id.output_text);

    progressBar = (ProgressBar) findViewById(R.id.crypto_process_progress_bar);
    progressBar.setVisibility(View.INVISIBLE);

    saveBackToSourceCheckBox = (CheckBox) findViewById(R.id.decryption_save_back_checkbox);
    encryptButton = (Button) findViewById(R.id.encrypt_button);
    encryptButton.setOnClickListener(this);
    decryptButton = (Button) findViewById(R.id.decrypt_button);
    decryptButton.setOnClickListener(this);
  }

  @Override
  protected void onResume() {
    super.onResume();
    OperationCompletedBroadcastReceiver.registerListener(listenerTag, this);
    OperationFailedBroadcastReceiver.registerListener(listenerTag, this);
  }

  @Override
  protected void onPause() {
    OperationCompletedBroadcastReceiver.unregisterListener(listenerTag, this);
    OperationFailedBroadcastReceiver.unregisterListener(listenerTag, this);
    super.onPause();
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.key_offset_menu, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    if (item.getItemId() == R.id.action_view_key_data) {
      new KeyOffsetPresentationDialogFragment()
          .show(getFragmentManager(), KeyOffsetPresentationDialogFragment.class.getSimpleName());
      return true;
    }
    return super.onOptionsItemSelected(item);
  }

  @Override
  public void onClick(View v) {
    switch (v.getId()) {
      case R.id.encrypt_button:
        startEncryption();
        break;
      case R.id.decrypt_button:
        startDecryption();
        break;
    }
  }

  private void startEncryption() {
    Intent startEncryptionIntent = new Intent(this, EncryptionService.class);
    startEncryptionIntent.putExtra(EncryptionService.OPERATION, EncryptionService.Operation.ENCRYPT);
    startEncryptionIntent.putExtra(EncryptionService.SOURCE_FILE_PATH, inputFilePathEditText.getText().toString());
    startEncryptionIntent.putExtra(EncryptionService.KEY_FILE_PATH, keyFilePathEditText.getText().toString());
    startEncryptionIntent.putExtra(EncryptionService.OFFSET_EXTRA, getStartingKeyOffset());
    startEncryptionIntent.putExtra(EncryptionService.DEST_FILE_PATH, outputFilePathEditText.getText().toString());
    startService(startEncryptionIntent);
    progressBar.setVisibility(View.VISIBLE);
    updateButtonsAndProgressBar(true);
    operationInProgress = EncryptionService.Operation.ENCRYPT;
  }

  private long getStartingKeyOffset() {
    long requestedOffsetFromLastPosition =
        keyOffsetEditText.getText().length() > 0 ? Long.parseLong(keyOffsetEditText.getText().toString()) : 0;
    long lastPositionInKey = controller.getOffsetForKeyWithPath(keyFilePathEditText.getText().toString());
    return lastPositionInKey + requestedOffsetFromLastPosition;
  }

  private void startDecryption() {
    Intent stopEncryptionIntent = new Intent(this, EncryptionService.class);
    stopEncryptionIntent.putExtra(EncryptionService.OPERATION, EncryptionService.Operation.DECRYPT);
    stopEncryptionIntent.putExtra(EncryptionService.SOURCE_FILE_PATH, inputFilePathEditText.getText().toString());
    stopEncryptionIntent.putExtra(EncryptionService.KEY_FILE_PATH, keyFilePathEditText.getText().toString());
    stopEncryptionIntent.putExtra(EncryptionService.SAVE_BACK_EXTRA, saveBackToSourceCheckBox.isChecked());
    stopEncryptionIntent.putExtra(EncryptionService.DEST_FILE_PATH, outputFilePathEditText.getText().toString());
    startService(stopEncryptionIntent);
    updateButtonsAndProgressBar(true);
    operationInProgress = EncryptionService.Operation.DECRYPT;
  }

  @Override
  public void onOperationCompleted(long newKeyOffset, String inputText, String outputText) {
    updateButtonsAndProgressBar(false);
    if (hasKeyAdvancedForEncrypt(newKeyOffset) || operationInProgress != EncryptionService.Operation.ENCRYPT) {
      KeyOffset key = getUpdatedKeyOffset(newKeyOffset);
      controller.addOrUpdateKey(key);
      inputTextView.setText(inputText);
      outputTextView.setText(outputText);
      Toast.makeText(this, "New key offset is " + newKeyOffset, Toast.LENGTH_LONG).show();
    }
    else {
      inputTextView.setText(inputText);
      outputTextView.setText("Could not encrypt file");
      Toast.makeText(this, "Key did not advance, not enough key was left to encrypt file", Toast.LENGTH_LONG).show();
    }
    operationInProgress = EncryptionService.Operation.NONE;
  }

  private boolean hasKeyAdvancedForEncrypt(long newKeyOffset) {
    return operationInProgress == EncryptionService.Operation.ENCRYPT && newKeyOffset != getStartingKeyOffset();
  }

  @Override
  public void onOperationFailed(String errorMessage) {
    updateButtonsAndProgressBar(false);
    inputTextView.setText("Operation failed");
    outputTextView.setText("Operation failed");
    Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show();
    operationInProgress = EncryptionService.Operation.NONE;
  }

  private KeyOffset getUpdatedKeyOffset(long newKeyOffset) {
    KeyOffset key = new KeyOffset();
    key.setKeyFilePath(keyFilePathEditText.getText().toString());
    key.setKeySize(new File(keyFilePathEditText.getText().toString()).length());
    key.setOffset(newKeyOffset);
    return key;
  }

  private void updateButtonsAndProgressBar(boolean isWorking) {
    encryptButton.setEnabled(!isWorking);
    decryptButton.setEnabled(!isWorking);
    progressBar.setVisibility(isWorking ? View.VISIBLE : View.INVISIBLE);
  }
}
