package com.iquestgroup.ndkonetimepad.application;

import android.app.Application;

/**
 * Application class used to load the native library on app start-up.
 *
 * @author victor.rad on 26.01.2015.
 */
public class EncryptionApplication extends Application {

  @Override
  public void onCreate() {
    super.onCreate();
    System.loadLibrary("onetimepad");
  }
}
