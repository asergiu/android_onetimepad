/* 
OTP varianta 1.2
one time pad full duplex
offset = variabil, pozitiv sau negativ
md5 works
optimized buffer size
abort key when reached keysize/2
key file cannot be overwritten
*/
#include <stdio.h>
#include <unistd.h>
#include <string.h>

#define FNSZ 200 // file name size
#define BUFSZ 200000 // buffer size
#define SHOWVERSION true // display version information: true or false
#define AUTH false // perform authentication procedure: true or false

#include "md.h"

// verifica egalitatea a doua secvente de lungime data
bool equ(unsigned char * a, unsigned char * b, int n)
{
for(int i=0;i<n;i++)
	if(a[i] != b[i]) return false;
return true;
}
// calc lungimea unui sir de "unsigned char"
unsigned int len(unsigned char *str)
{
unsigned int i=0;
while(str[i]!=0) i++;
return i;
}

///////////////////////////////////
// Programul principal
///////////////////////////////////

int main()
{
char ns[FNSZ]; /* nume fisier sursa */
char nd[FNSZ]; /* nume fisier destinatie */
char nk[FNSZ]; /* nume fisier cheie */
FILE * fs; /* pointer la fisierul sursa */
FILE * fd; /* pointer la fisierul destinatie */
FILE * fk; /* pointer la fisierul cheie */
FILE * ff; // pointer la fisierul offset
unsigned char buffer[BUFSZ]; /* buffer de manevra, aici citesc din fis intr */
unsigned char cheie[BUFSZ]; /* cheia de cifrare */
long dimbuf; // grad de umplere buffer 
long dimch;  // grad de umplere cheie
long offset; // offset cheie
long inputsz; // dim fis de intrare
long keysz; // dim fis cheie
long backoffset; // backup offset daca e negativ
long i; // loop counter

unsigned char user[30]; //username
unsigned char pass[30]; //password
unsigned char mode; // e = encrypt, d = decrypt 

unsigned char mduser[16]={47,251,155,52,152,65,113,216,206,157,76,231,236,208,99,210};
unsigned char md1[16];
unsigned char mdpass[16]={36,232,133,154,49,16,14,85,77,175,202,51,133,32,59,145};
unsigned char md2[16];

unsigned char mdenc[16]={225,103,23,151,197,46,21,247,99,56,11,69,232,65,236,50};
unsigned char mddec[16]={130,119,224,145,13,117,1,149,180,72,121,118,22,224,145,173};
unsigned char md3[16];

/////////////////////////////////////////////////////////////////
// print version number
/////////////////////////////////////////////////////////////////
if(SHOWVERSION)
{
printf("---------------------------------------\n");
printf("OTP version 1.2 build 13.09.2007\n");
printf("---------------------------------------\n");
}
/////////////////////////////////////////////////////////////////
// Autenthication checking procedure
/////////////////////////////////////////////////////////////////
if(AUTH)
{
// Username
printf("1. Who are you, really? ");
scanf("%s",user); // read username
MD5String(user,len(user),md1);
if(!equ(mduser, md1,16)) exit(1); 
// Password
printf("2. Your password is: udyamo ... ");
scanf("%s",pass); // read pass
MD5String(pass,len(pass),md2);
if(!equ(mdpass, md2,16)) exit(1); 
/////////////////////////////////////////////////////////////////
// Action request 
/////////////////////////////////////////////////////////////////
printf("3. What do you want, %s? ",user);
scanf("%s",mode); // read mode
MD5String(mode,len(mode),md3);
if(!equ(mdenc, md3,16) && !equ(mddec, md3,16)) exit(1); 
}
else
{
/////////////////////////////////////////////////////////////////
// just Action request 
/////////////////////////////////////////////////////////////////
printf("What do you want? ");
scanf("%c",&mode); // read mode
//MD5String(mode,len(mode),md3);
//if(!equ(mdenc, md3,16) && !equ(mddec, md3,16)) exit(1); 
}
/////////////////////////////////////////////////////////////////
// Encryption
/////////////////////////////////////////////////////////////////

if((mode == 'e')||(mode=='E')) // encrypt
{
printf("Encrypting ...\n");
// Get source file
printf("Source filename:");
scanf("%s",ns); /* citire nume fisier sursa */
// Get destination file
printf("Destination filename:");
scanf("%s",nd); /* citire nume fisier destinatie */
// Get key file
printf("Key filename:");
scanf("%s",nk); /* citire nume fisier cheie */
// open source file
fs=fopen(ns, "rb"); /* deschidere fisier sursa */
if(fs==NULL) {printf("Error opening file: %s\n", ns); exit(1);}
// open destination file
// check for key != destination
if(!strcmp(nd, nk)) {printf("Key file is the destination file. Not allowed!\n"); exit(1);}
fd=fopen(nd, "w+b"); /* deschidere fisier destinatie */
if(fd==NULL) {printf("Error opening file: %s\n", nd); exit(1);}
// open key file
fk=fopen(nk, "rb"); /* deschidere fisier cheie */
if(fk==NULL) {printf("Error opening file: %s\n", nk); exit(1);}
// Recover the offset from Offset.txt file
ff=fopen("offset.txt", "rb"); /* deschidere offset file */
if(ff==NULL) 
	{
	printf("Offset file does not exist, create file <offset.txt> !\n"); 
	exit(1);
	}
else
	{
	fscanf(ff,"%ld",&offset);
	fclose(ff);
	}
printf("Initial offset is: %ld\n",offset);
//det lungime fis sursa
fseek(fs, 0, SEEK_END); // merg la sf fis sursa
inputsz = ftell(fs); // determin lungime fisier sursa
fseek(fs, 0, SEEK_SET); // revin la inceput fis sursa
//det lungime fis sursa
fseek(fk, 0, SEEK_END); // merg la sf fis cheie
keysz = ftell(fk); // determin lungime fisier cheie
fseek(fk, 0, SEEK_SET); // revin la inceput fis cheie

//iesire fortata daca se depaseste jumatatea keii
if(abs(offset)+inputsz > (keysz/2))
{	
	printf("This key (%s) is consumed! Use a new key! I refuse to encrypt!\n", nk); 
	fclose(fd);
    unlink(nd);
	exit(1);
}

// Set key position (offset) pozitionare in fis cheie
if(offset >= 0)
	fseek(fk,offset,SEEK_SET);
else
{

	offset -= inputsz;	// ajustez offsetul
	printf("Offset set to %ld\n", offset);
	backoffset = offset; // si il memorez
	fseek(fk, offset,SEEK_END); // pozitionez in cheie 
}
// Write offset in the destination file
fprintf(fd,"%ld",offset);fprintf(fd," ");
// Write initial filename in the destination file
fprintf(fd,"%s",ns);fprintf(fd," ");
// Main loop
while(true)
	{	
	dimbuf=fread(buffer, 1, BUFSZ, fs);
	if(dimbuf == -1) {printf("Error reading source!!!\n");break;}
	dimch=fread(cheie, 1, dimbuf, fk);
	if(dimch == -1) {printf("Error reading key!!!\n");break;}
	if(dimbuf != dimch) {printf("Synchnization error!!! Not enough key data.\n");break;}
	if(dimbuf == 0) break;
    for(i=0; i<dimbuf; i++)
		buffer[i] ^= cheie[i]; /* aplicare xor */
    fwrite(buffer, 1, dimbuf, fd);
	offset += dimbuf;
	printf("Offset increased to: %ld\n",offset);
	} // end while (main loop)
// update offset file
ff=fopen("offset.txt","wb+");
if(ff == NULL)
	{
	printf("Error setting the new offset: could not open offset.txt\n");
	exit(1);
	}
else
	{
	if(offset > 0)
		{printf("New offset is: %ld\n",offset);	fprintf(ff,"%ld",offset);}
	else
		{printf("New offset is: %ld\n",backoffset);	fprintf(ff,"%ld",backoffset);}
	fclose(ff); // close offset file
	}
// closing files
fclose(fs); // close source file
fclose(fd); // close destination file
fclose(fk); // close key file
} 
// end encryption

/////////////////////////////////////////////////////////////////
// Decryption
/////////////////////////////////////////////////////////////////

if(mode == 'd')||(mode=='D')) // decrypt
{
printf("Decrypting ...\n");
// Get source file
printf("Source filename:");
scanf("%s",ns); /* citire nume fisier sursa */
fs=fopen(ns, "rb"); /* deschidere fisier sursa */
if(fs==NULL) {printf("Error opening file: %s\n", ns); exit(1);}
// Get key file
printf("Key filename:");
scanf("%s",nk); /* citire nume fisier cheie */
fk=fopen(nk, "rb"); /* deschidere fisier cheie */
if(fk==NULL) {printf("Error opening file: %s\n", nk); exit(1);}
// recover offset
fscanf(fs,"%ld",&offset);
// recover destination filename
fscanf(fs,"%s",nd);
//advance one byte
fseek(fs,1,SEEK_CUR);
//ask for confirmation on dest filename
printf("The destination filename is %s.\n",nd);
printf("If this file exists already, it will be destroyed!\n");
printf("Do you want to change the destination filename?(y/n)\n");
char choice='h';
while((choice != 'y') && (choice != 'n'))scanf("%c",&choice);
if(choice == 'y')
{// Get destination file
	printf("Destination filename: ");
	scanf("%s",nd); /* citire nume fisier destinatie */
}
// open destination file
fd=fopen(nd, "w+b"); /* deschidere fisier destinatie */
if(fd==NULL) {printf("Error opening file: %s\n", nd); exit(1);}
// set key offset
if(offset >= 0)
	fseek(fk, offset, SEEK_SET);
else
	fseek(fk, offset, SEEK_END);
// Main loop
while(true)
{	
	dimbuf=fread(buffer, 1, BUFSZ, fs);
	if(dimbuf == -1) {printf("Error reading source!!!\n");break;}
	dimch=fread(cheie, 1, dimbuf, fk);
	if(dimch == -1) {printf("Error reading key!!!\n");break;}
	if(dimbuf != dimch) {printf("Synchronization error!!! Not enough key data.\n");break;}
	if(dimbuf == 0) break;
    for(i=0; i<dimbuf; i++)
		buffer[i]^=cheie[i]; /* aplicare xor */
    fwrite(buffer, 1, dimbuf, fd);
	offset +=dimbuf;
	printf("offset = %ld\n",offset);
} // end while (main loop)
// verify offset overlap
/*long doffset;
ff=fopen("offset.txt","rb+");
if(ff != NULL)
	{
	fscanf(ff,"%ld",&doffset);
	if((offset < 0) && (doffset >= 0) && ()
		{
		printf("New offset is: %ld\n",offset);
		}
	fclose(ff);// close offset file
	}
else
{
	printf("error opening / creating offset file\n");
}*/
// closing files
fclose(fs); // close source file
fclose(fd); // close destination file
fclose(fk); // close key file
} 
// end decryption
}
// end program
